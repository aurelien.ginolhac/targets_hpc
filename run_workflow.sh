#!/bin/bash
#SBATCH --partition=interactive
#SBATCH --job-name=targets_test
#SBATCH -o targets_test.out
#SBATCH --time=0-00:10:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=ALL
#SBATCH --mail-user=aurelien.ginolhac@uni.lu
#SBATCH --mem=2048


# Working dir
echo $PWD

echo "Run script run_workflow.R"

Rscript --vanilla run_workflow.R
